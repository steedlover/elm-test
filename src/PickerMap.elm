module PickerMap exposing
    ( Address
    , AddressesArrElement
    , Coordinates
    , Msg(..)
    , PickerModel
    , decodeAddressesArrElement
    , emptyAddress
    , emptyCoords
    , fetchPointAddress
    , init
    , showTipName
    , update
    , view
    )

import Html exposing (Html, div)
import Html.Attributes exposing (classList, id)
import Http
import Json.Decode as Decode
import Json.Decode.Pipeline exposing (optional)
import Json.Encode as Encode
import PlacepickerConfig exposing (apiKey)
--import Result



-- import Debug


type Msg
    = SetCoords Coordinates
    | GotPoint (Result Http.Error Response)
    | SetAddress AddressesArrElement
    | ToggleLoading Bool


type alias Coordinates =
    { lat : Float
    , lon : Float
    }


type alias PickerModel =
    { coords : Coordinates
    , mapOpen : Bool
    , zoom : Int
    , mapid : String
    , mapclass : String
    , address : Maybe AddressesArrElement
    , loading : Bool
    }


type alias Response =
    { summary : Summary
    , addresses : List AddressesArrElement
    }


type alias Summary =
    { queryTime : Int
    , numResults : Int
    }


type alias AddressesArrElement =
    { type_ : Maybe String
    , address : Address
    , position : Coordinates
    , poi : Maybe Poi
    }


type alias Poi =
    { name : String
    }


type alias Address =
    { buildingNumber : String
    , streetNumber : String
    , street : String
    , streetName : String
    , countryCode : String
    , countrySubdivision : String
    , countrySubdivisionName : String
    , countrySecondarySubdivision : String
    , countryTertiarySubdivision : String
    , municipality : String
    , postalCode : String
    , municipalitySubdivision : String
    , country : String
    , freeformAddress : String
    , matchType : String
    }


emptyCoords : Coordinates
emptyCoords =
    { lat = 0
    , lon = 0
    }


emptyAddress : Address
emptyAddress =
    { buildingNumber = ""
    , streetNumber = ""
    , street = ""
    , streetName = ""
    , countryCode = ""
    , countrySubdivision = ""
    , countrySubdivisionName = ""
    , countrySecondarySubdivision = ""
    , countryTertiarySubdivision = ""
    , municipality = ""
    , postalCode = ""
    , municipalitySubdivision = ""
    , country = ""
    , freeformAddress = "No result"
    , matchType = ""
    }


strToCoords : String -> Coordinates
strToCoords str =
    let
        arr =
            String.split "," str

        lat =
            List.head arr |> Maybe.withDefault "" |> String.toFloat |> Maybe.withDefault 0

        lon =
            List.reverse arr |> List.head |> Maybe.withDefault "" |> String.toFloat |> Maybe.withDefault 0
    in
    { lat = lat, lon = lon }


fetchPointAddress : Coordinates -> Cmd Msg
fetchPointAddress coords =
    Http.get
        { url =
            "https://api.tomtom.com/search/2/reverseGeocode/"
                ++ String.fromFloat coords.lat
                ++ ","
                ++ String.fromFloat coords.lon
                ++ ".json?key="
                ++ apiKey
                ++ "&language=en-US"
        , expect = Http.expectJson GotPoint decodeJson
        }



-- INIT


init : PickerModel -> ( PickerModel, Cmd Msg )
init model =
    ( model, Cmd.none )



-- DECODERS


decodeJson : Decode.Decoder Response
decodeJson =
    Decode.map2 Response
        (Decode.field "summary" decodeSummary)
        (Decode.field "addresses" (Decode.list decodeAddressesArrElement))


decodeAddressesArrElement : Decode.Decoder AddressesArrElement
decodeAddressesArrElement =
    Decode.map4 AddressesArrElement
        (Decode.maybe <| Decode.field "type" Decode.string)
        (Decode.field "address" decodeAddress)
        (Decode.field "position" Decode.value |> Decode.andThen positionDecoder)
        (Decode.maybe <| Decode.field "poi" poiDecoder)


poiDecoder : Decode.Decoder Poi
poiDecoder =
    Decode.map Poi
        (Decode.field "name" Decode.string)


decodeCoordinates : Decode.Decoder Coordinates
decodeCoordinates =
    Decode.map2 Coordinates
        (Decode.field "lat" Decode.float)
        (Decode.field "lon" Decode.float)


positionDecoder : Decode.Value -> Decode.Decoder Coordinates
positionDecoder result =
    case Decode.decodeString Decode.string (Encode.encode 0 result) of
        Ok str ->
            Decode.succeed <| strToCoords str

        Err _ ->
            case Decode.decodeValue decodeCoordinates result of
                Ok coords ->
                    Decode.succeed coords

                Err _ ->
                    Decode.succeed emptyCoords


decodeSummary : Decode.Decoder Summary
decodeSummary =
    Decode.map2 Summary
        (Decode.field "queryTime" Decode.int)
        (Decode.field "numResults" Decode.int)


decodeAddress : Decode.Decoder Address
decodeAddress =
    Decode.succeed Address
        |> optional "buildingNumber" Decode.string ""
        |> optional "streetNumber" Decode.string ""
        |> optional "street" Decode.string ""
        |> optional "streetName" Decode.string ""
        |> optional "countryCode" Decode.string ""
        |> optional "countrySubdivision" Decode.string ""
        |> optional "countrySubdivisionName" Decode.string ""
        |> optional "countrySecondarySubdivision" Decode.string ""
        |> optional "countryTertiarySubdivision" Decode.string ""
        |> optional "municipality" Decode.string ""
        |> optional "postalCode" Decode.string ""
        |> optional "municipalitySubdivision" Decode.string ""
        |> optional "country" Decode.string ""
        |> optional "freeFormAddress" Decode.string ""
        |> optional "matchType" Decode.string ""



-- UPDATE


update : Msg -> PickerModel -> ( PickerModel, Cmd Msg )
update msg model =
    case msg of
        SetCoords coords ->
            let
                v =
                    coords

                -- |> Debug.log "child coords"
            in
            ( { model | loading = True, coords = coords, mapOpen = True }, fetchPointAddress coords )

        SetAddress res ->
            let
                address =
                    res.address

                --|> Debug.log "PickerMap.SetAddress"
            in
            ( { model
                | mapOpen = True
                , loading = False
                , address =
                    Just
                        { poi = res.poi
                        , type_ = res.type_
                        , address = { address | freeformAddress = showTipName res.address }
                        , position = res.position
                        }
              }
            , Cmd.none
            )

        ToggleLoading bool ->
            ( { model | loading = bool }, Cmd.none )

        GotPoint _ ->
            ( { model | loading = False }, Cmd.none )



-- VIEW


showTipName : Address -> String
showTipName el =
    let
        resultArr =
            case el |> .freeformAddress |> String.isEmpty of
                True ->
                    [ String.join " " [ el.streetNumber, el.streetName ]
                    , el.countryTertiarySubdivision
                    , el.postalCode
                    ]

                False ->
                    [ el.freeformAddress ]

        resultString =
            case resultArr of
                [] ->
                    ""

                _ ->
                    List.map String.trim resultArr
                        |> List.filter (not << String.isEmpty)
                        |> String.join ", "
    in
    resultString


view : PickerModel -> Html Msg
view model =
    div
        [ classList
            [ ( model.mapclass, True )
            , ( "open", model.mapOpen )
            ]
        , id model.mapid
        ]
        []
