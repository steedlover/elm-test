port module Multiverse exposing (..)

import Browser
import Html exposing (Html, form, h1, h2, h3, button, div, input, text)
import Html.Attributes exposing (type_, class, id, placeholder, value, disabled)
import Html.Attributes.Aria exposing (role, ariaLabel)
import Html.Events exposing (onClick, onInput)
import Array exposing (push, fromList, toList)
import Debug exposing (log)

type alias Flags =
    Model

type alias Data =
    List ( List String )

type alias Model =
    { data: Data
    , blockTitle: String
    , maxRows: Int
    }

type alias EmptyRow =
    List String

emptyRow : EmptyRow
emptyRow = "" :: "" :: []

main : Program (Maybe Flags) Model Msg
main =
    Browser.element {
        init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
    }


init : Maybe Flags -> ( Model, Cmd Msg )
init initFlags =
    let
        val =
            case initFlags of
                Maybe.Just flags ->
                    flags

                Nothing ->
                    { data = emptyRow :: []
                    , blockTitle = ""
                    , maxRows = 2
                    }
    in
        ( val, Cmd.none )


-- UPDATE


type Msg
    = Add
    | Reset
    | Set Model
    | FieldChanged Int Int String

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Add ->
            if List.length model.data < model.maxRows then
                let
                    newModel =
                        { model | data =
                        Array.push emptyRow (Array.fromList model.data)
                        |> toList
                        --|> Debug.log "new data add"
                        }
                in
                    ( newModel, changed newModel.data )
            else
                ( model, Cmd.none)

        Reset ->
            let
                newModel =
                    { model | data =
                    List.singleton emptyRow
                    --|> Debug.log "new data reset"
                    }
            in
                ( newModel, changed newModel.data )

        Set val ->
            let
                newModel =
                    val
                    --|> Debug.log "model"
            in
                ( newModel, Cmd.none )

        FieldChanged row cell str ->
            let
                newModel =
                    { model | data =
                    let
                        toggle i val =
                            if i == row then
                                let
                                    subtoggle j subval =
                                        if j == cell then
                                            str
                                        else
                                            subval
                                in
                                    List.indexedMap subtoggle val
                            else
                                val
                    in
                        List.indexedMap toggle model.data
                    --|> Debug.log "new model"
                    }
            in
                ( newModel, changed newModel.data )


port setList : (Model -> msg) -> Sub msg

port changed : Data -> Cmd msg

subscriptions : Model -> Sub Msg
subscriptions model =
    setList Set


-- VIEW

renderList : List String -> Int -> Html Msg
renderList lst i =
    lst
        |> List.indexedMap (\j -> \l ->
            div [ class "col" ]
            [ input
                [ class "form-control"
                , placeholder "Text to reverse"
                , value l
                , onInput (FieldChanged i j)
                ] []
            ]
        )
        |> div [ class "form-row form-group" ]

view : Model -> Html Msg
view model =
    div [ class "form-block"]
        [ div [ class "form-group" ]
            [ h2 [] [ text model.blockTitle ] ]
        , div [ class "form-group" ] ( List.indexedMap (\i -> \l -> renderList l i ) model.data )
        , div [ class "form-group" ]
            [ div [ class "form-row" ]
                [ div [ class "col" ]
                    [ button
                        [ type_ "button"
                        , class "btn btn-success"
                        , onClick Add
                        , disabled (List.length model.data >= model.maxRows)
                        ] [ text "Add" ]
                    , button [ type_ "reset", class "btn btn-danger", onClick Reset] [ text "Reset" ]
                    ]
                ]
            ]
        ]
