Vue.component("placepicker", {
  data: function() {
    return {
      _,
      hints: [],
      hintsVisible: false,
      hintsLoading: false,
      zoom: 14,
      coords: [35.894095, 14.506422],
      searchString: "",
      timeout: null,
      // apiKey: "ge-7b62d356f5336058",
      apiKey: 'C0A9K6Gjb8Jz9GDAdvBUpTtYAQpToe1c',
      // mapBoxToken: 'pk.eyJ1Ijoic3RlZWRsb3ZlciIsImEiOiJjazZraXR6NHowNGgwM21wYXJvNjRwNzNtIn0.ntVe-IDaNcelHEj1sXqPtA',
      myMap: null,
      myMapLayer: null,
      mapContainerId: "mapid",
      tileUrl: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
      mapOptions: {
        maxZoom: 18,
        attribution:
          'Map data &copy <a href="https://openstreetmap.org">OpenStreetMap</a> contributors'
      },
      // yx: L.latLng
    };
  },
  // watch: {
  //     coords: {
  //         handler: function(newVal, oldVal) {
  //             if (!_.isEqual(newVal, oldVal)) {
  //                 this.setMapView(newVal, 12);
  //             }
  //         },
  //         deep: true
  //     }
  // },
  template: `
        <div>
            <div class="form-block">
                <div id="mapid"></div>
                <div class="dropdown">
                    <input type="text" class="form-control dropdown-toggle autocomplete-input" v-bind:class="{'loading': hintsLoading}" v-model="searchString" @input="newSearch()" placeholder="Input address" id="vueDropdownMenuLink" />
                    <div class="dropdown-menu" v-bind:class="{'active': hintsVisible}" aria-label="vueDropdownMenuLink">
                        <div v-if="hints.length > 0">
                            <!--<a class="dropdown-item" v-for="hint in hints" v-on:click="hintClick(hint)">{{hint.properties.label}}</a>-->
                            <a class="dropdown-item" v-for="hint in hints" v-on:click="hintClick(hint)">{{hint.address.freeformAddress}}</a>
                        </div>
                        <div v-else>
                            <a class="dropdown-item">No result</a>
                        </div>
                    </div>
                </div>
            </div>        
        </div>
    `,
  mounted() {
    // mount map
    var tiles = new L.tileLayer(this.tileUrl, this.mapOptions);
    this.myMap = L.map(this.mapContainerId)
      .addLayer(tiles)
      .setView(this.coords, this.zoom);
    // Add click on map listener
    this.myMap.on('click', e => {
      var coord = e.latlng;
      this.searchByCoords({lat: coord.lat, lng: coord.lng});
    });
  },
  methods: {
    newSearch: function() {
      var vm = this;
      if (this.timeout) clearTimeout(this.timeout);
      this.timeout = setTimeout(function() {
        if (vm.searchString !== "") {
          vm.fetchHints(vm.searchString);
        } else {
          vm.hintsVisible = false;
          vm.clearHints();
        }
      }, 500);
    },
    fetchHints: function(str) {
      var vm = this,
        arr = str.split(" "),
        // uri =
        //   "https://api.geocode.earth/v1/autocomplete?api_key=" +
        //   this.apiKey +
        //   "&text=" +
        //   arr.join("+") +
        //   "&sources=osm,oa,gn,wof&layers=address,venue,street,locality";
        uri = 'https://api.tomtom.com/search/2/search/' + arr.join(" ") + '.json?key=' + this.apiKey + '&language=en-US&idxSet=Xstr,Str,POI,PAD,Addr&limit=10';
      vm.hintsLoading = true;
      axios.get(uri).then(response => {
        console.log(response);
        // if (
        //   response &&
        //   typeof response === "object" &&
        //   response.data &&
        //   response.data.features.length
        // ) {
        if (response && typeof response === 'object' && response.data && response.data.results.length) {
          vm.hints = response.data.results;
        } else {
          this.clearHints();
        }
        vm.hintsVisible = true;
        vm.hintsLoading = false;
      });
    },
    clearHints: function() {
      this.hints = [];
    },
    // long2tile(lon, zoom) {
    //   return Math.floor(((lon + 180) / 360) * Math.pow(2, zoom));
    // },
    // lat2tile(lat, zoom) {
    //   return Math.floor(
    //     ((1 -
    //       Math.log(
    //         Math.tan((lat * Math.PI) / 180) +
    //           1 / Math.cos((lat * Math.PI) / 180)
    //       ) /
    //         Math.PI) /
    //       2) *
    //       Math.pow(2, zoom)
    //   );
    // },
    // fetchLayer(coords) {
    //     var vm =this,
    //         uri = 'https://api.geocode.earth/v1/reverse?point.lat=' + coords[0] + '&point.lon=-' + coords[1] + '&&boundary.country=GBR&api_key='+this.apiKey;
    //     vm.hintsLoading = true;
    //     axios.get(uri)
    //         .then(response => {
    //             if (response && this.myMap && typeof response === 'object' && response.data.type === 'FeatureCollection' && response.data.features) {
    //                 console.log(response.data, 'layers response');
    //                 L.geoJSON(response.data).addTo(this.myMap);
    //             }
    //             vm.hintsLoading = false;
    //         });
    // },
    // xy: function(x, y) {
    //   if (L.Util.isArray(x)) {
    //     // When doing xy([x, y]);
    //     return this.yx(x[1], x[0]);
    //   }
    //   return this.yx(y, x); // When doing xy(x, y);
    // },
    setMapView: function(data) {
      let obj
        // coords = data.geometry.coordinates,
        , coords = data.position
        , zoom = 16
        , pointName
        , pointDesc
        , addressObj = this.assembleAddressObj(data.address)
        // , recStyle = {
        //   color: "orange",
        //   dashArray: "3",
        //   fillColor: "#ff7800",
        //   fillOpacity: 0.8,
        //   weight: 2
        // }
        ;
      if (
        zoom > 0 &&
        zoom <= this.mapOptions.maxZoom
      ) {
        console.log(data, "hint");

        // L.geoJSON(
        //   {
        //     type: "Feature",
        //     properties: { party: "Republican" },
        //     geometry: {
        //       type: "Polygon",
        //       coordinates: [
        //         [
        //           [-104.05, 48.99],
        //           [-97.22, 48.98],
        //           [-96.58, 45.94],
        //           [-104.03, 45.94],
        //           [-104.05, 48.99]
        //         ]
        //       ]
        //     }
        //   },
        //   {
        //     style: {
        //       color: "blue",
        //       dashArray: "3",
        //       fillColor: "#cc0011",
        //       fillOpacity: "0.8",
        //       weight: "5"
        //     }
        //   }
        // ).addTo(this.myMap);

        // if (_.isArray(data.bbox) && data.bbox.length === 4) {
        //   L.rectangle(
        //     [
        //       [data.bbox[1], data.bbox[0]],
        //       [data.bbox[3], data.bbox[2]]
        //     ],
        //     recStyle
        //   ).addTo(this.myMap);
        // }

        // this.myMapLayer.addData(data).eachLayer(
        //     function (featureInstancelayer) {
        //         propertyValue = featureInstancelayer.feature.properties['layer'];
        //
        //         // Your function that determines a fill color for a particular
        //         // property name and value.
        //         var myFillColor = getColor('layer', propertyValue);
        //
        //         featureInstancelayer.setStyle({
        //             fillColor: '#cc0011',
        //             fillOpacity: 0.8,
        //             weight: 0.5
        //         });
        //     }
        // );
        if (this.myMapLayer === null) {
          console.log('new point');
          this.myMapLayer = L.marker(coords).addTo(this.myMap);
        } else {
          console.log('change coords');
          this.myMapLayer.setLatLng(coords);
        }
        if (data.poi && data.poi.name) {
          pointName = data.poi.name;
        }
        if (data.address && data.address.freeformAddress) {
          pointDesc = data.address.freeformAddress;
        }
        if (pointName || pointDesc) {
          this.myMapLayer
            .bindPopup((pointName ? "<b>" + pointName + "</b><br />" : "") + (pointDesc ? pointDesc : ""))
            .openPopup();
        }
        this.$parent.setAddress(addressObj);
        this.myMap.panTo(coords, zoom);
        this.searchString = addressObj.freeformAddress;
      }
    },
    hintClick: function(hint) {
      try {
        // this.coords = hint.geometry.coordinates;
        this.hintsVisible = false;
        // this.searchString = hint.properties.label;
        this.setMapView(hint);
        // console.log(hint, 'hint');
        // this.searchByName(this.searchString, hint.properties.region_gid, hint.properties.layer);
      } catch (e) {
        console.warn(e);
      }
    },
    // searchByName: function(str, gid, layer) {
    //   var vm = this,
    //     arr = str.split(" "),
    //     uri =
    //       "https://api.geocode.earth/v1/search?api_key=" +
    //       this.apiKey +
    //       "&text=" +
    //       str +
    //       "&boundary.gid=" +
    //       gid +
    //       "&layers=" +
    //       layer;
    //   vm.hintsLoading = true;
    //   axios.get(uri).then(response => {
    //     console.log(response, "search response");
    //     if (
    //       response &&
    //       typeof response === "object" &&
    //       response.data &&
    //       response.data.features.length
    //     ) {
    //       vm.hints = response.data.features;
    //       vm.hintsVisible = true;
    //     } else {
    //       vm.clearHints();
    //     }
    //     vm.hintsLoading = false;
    //   });
    // }
    searchByCoords: function(coords) {
      var vm = this,
        newPoint,
        uri = 'https://api.tomtom.com/search/2/reverseGeocode/' + coords.lat + ',' + coords.lng + '.json?key=' + this.apiKey + '&language=en-US';
      vm.hintsLoading = true;
      axios.get(uri).then(response => {
        if (response.data && response.data.addresses.length) {
          newPoint = response.data.addresses[0];
          console.log(newPoint, 'new point#0');
          if (typeof newPoint.position === 'string') {
            newPoint.position = this.coordsArrToObj(newPoint.position.split(','));
          }
          this.setMapView(newPoint);
        }
        vm.hintsLoading = false;
      });
    },
    coordsArrToObj: function(arr) {
      return {
        lat: arr[0],
        lng: arr[1]
      };
    },
    assembleAddressObj: function(address) {
      var obj = {
        freeformAddress: ''
      };
      if (address.postalCode) {
        obj.zip = address.postalCode;
        if (obj.freeformAddress.length > 0) obj.freeformAddress += ', ';
        obj.freeformAddress += address.postalCode;
      }
      if (address.localName) {
        obj.city = address.localName;
        if (obj.freeformAddress.length > 0) obj.freeformAddress += ', ';
        obj.freeformAddress += obj.city;
      }
      if (address.streetNameAndNumber) {
        obj.address = address.streetNameAndNumber;
      } else if (address.streetName) {
        obj.address = address.streetName;
        if (address.streetNumber) {
          obj.address += ' ' + address.streetNumber;
        }
      }
      if (obj.address) {
        if (obj.freeformAddress.length > 0) obj.freeformAddress += ', ';
        obj.freeformAddress += obj.address;
      }
      if (address.countryCode) {
        obj.countryCode = address.countryCode;
      }
      if (address.country) {
        obj.country = address.country;
      }
      if (address.postalCode) {
        obj.zip = address.postalCode;
      }
      return obj;
    }
  }
});
