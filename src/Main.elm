module Main exposing (..)

-- Press buttons to increment and decrement a counter.
--
-- Read how it works:
--   https://guide.elm-lang.org/architecture/buttons.html


import Browser
import Html exposing (Html, button, div, ul, li, text)
import Html.Attributes exposing (class, id)
import Html.Events exposing (onClick)
import Array exposing (..)
--import List.Extra exposing (swapAt, remove, getAt)
--import Debug exposing (..)

-- MAIN

main =
    Browser.sandbox { init = init, update = update, view = view }

-- MODEL

type alias Model =
    {counter : Int, list : List String}

init : Model
init =
    { counter = 0
    , list = ["first", "second", "third"]
    }

-- UPDATE

type Msg
    = Increment
    | Decrement

update : Msg -> Model -> Model
update msg model =
    case msg of
        Increment ->
            { model | counter = model.counter + 1
            , list = swapListItems model.list True
            }

        Decrement ->
            { model | counter = model.counter - 1
            , list = swapListItems model.list False
            }

strFunc : String -> Int -> String
strFunc strValue index =
    strValue ++ " " ++ String.fromInt index

swapListItems : List String -> Bool -> List String
swapListItems list up =
    let
        arr : Array String
        arr = Array.fromList list

        len : Int
        len = Array.length arr

        lastEl : String
        lastEl = Array.get (len - 1) arr
            |> Maybe.withDefault ""

        firstEl : String
        firstEl = Array.get 0 arr
            |> Maybe.withDefault ""
    in
        arr
            |> Array.indexedMap (\i -> \e ->
                case up of
                    True ->
                        if i < ( len - 1 ) then
                            Maybe.withDefault ""
                                <| Array.get(i + 1) arr
                        else
                            firstEl
                    False ->
                        if i > 0 then
                            Maybe.withDefault ""
                                <| Array.get(i - 1) arr
                        else
                            lastEl
            )
            |> toList

-- VIEW


view : Model -> Html Msg
view model =
    div []
    [ button [ onClick Decrement ] [ text "-" ]
    , div [] [ text (String.fromInt model.counter) ]
    , button [ onClick Increment ] [ text "+" ]
    , ul [ class "list center" ]
        ( List.indexedMap (\i -> \l -> li [ class "list-item"] [ text <| strFunc l i ]) model.list )
    ]
