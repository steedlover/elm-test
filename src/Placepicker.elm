port module Placepicker exposing (..)

-- import Debug
--import Result

import Browser
import Debounce exposing (Debounce)
import Html exposing (Html, a, div, input, text)
import Html.Attributes exposing (class, classList, id, placeholder, type_, value)
import Html.Attributes.Aria exposing (ariaLabel)
import Html.Events exposing (onInput)
import Html.Lazy exposing (lazy)
import Http
import Json.Decode as Decode
import List
import PickerMap exposing (view)
import PlacepickerConfig exposing (apiKey)
import Task


main : Program (Maybe Flags) Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


emptyTip : PickerMap.AddressesArrElement
emptyTip =
    { address = PickerMap.emptyAddress
    , position = PickerMap.emptyCoords
    , poi = Nothing
    , type_ = Just "Empty"
    }


type alias Model =
    { search : String
    , error : String
    , tips : List PickerMap.AddressesArrElement
    , map : PickerMap.PickerModel
    , debounce : Debounce String
    }


type alias Summary =
    { query : String
    , numResults : Int
    , totalResults : Int
    }


type alias AutocompleteJson =
    { summary : Summary
    , results : List PickerMap.AddressesArrElement
    }


type alias Flags =
    { search : String
    , coordinates : PickerMap.Coordinates
    , zoom : Int
    , mapid : String
    , mapclass : String
    , address : Maybe PickerMap.AddressesArrElement
    }


init : Maybe Flags -> ( Model, Cmd Msg )
init initFlags =
    let
        flags =
            case initFlags of
                Maybe.Just f ->
                    f

                Nothing ->
                    { search = ""
                    , coordinates = PickerMap.emptyCoords
                    , zoom = 12
                    , mapid = "mapid"
                    , mapclass = "mapclass"
                    , address = Nothing
                    }

        ( mapInit, _ ) =
            PickerMap.init
                { coords = flags |> .coordinates
                , mapOpen = False
                , zoom = flags |> .zoom
                , mapid = flags |> .mapid
                , mapclass = flags |> .mapclass
                , address = flags |> .address
                , loading = False
                }

        val =
            { search = flags |> .search
            , error = ""
            , tips = []
            , map = mapInit
            , debounce = Debounce.init
            }
    in
    ( val, Cmd.none )



-- DECODERS


decodeAutocomplete : Decode.Decoder AutocompleteJson
decodeAutocomplete =
    Decode.map2 AutocompleteJson
        (Decode.field "summary" decodeSummary)
        (Decode.field "results" (Decode.list PickerMap.decodeAddressesArrElement))


decodeSummary : Decode.Decoder Summary
decodeSummary =
    Decode.map3 Summary
        (Decode.field "query" Decode.string)
        (Decode.field "numResults" Decode.int)
        (Decode.field "totalResults" Decode.int)



--decodeMaybeStringFieldWithDefault : String -> String -> Decode.Decoder String
--decodeMaybeStringFieldWithDefault field def =
--    Decode.field field Decode.string |> Decode.maybe |> Decode.map (Maybe.withDefault def)
-- UPDATE


fetchAddress : List String -> Cmd Msg
fetchAddress arr =
    Http.get
        { url =
            "https://api.tomtom.com/search/2/search/"
                ++ String.join " " arr
                ++ ".json?key="
                ++ apiKey
                ++ "&language=en-US&idxSet=Xstr,Str,POI,PAD,Addr&limit=10"
        , expect = Http.expectJson GotResult decodeAutocomplete
        }


type Msg
    = SearchChange String
    | DebounceSearch Debounce.Msg
    | TipsFetchExec String
    | TipClick PickerMap.AddressesArrElement
    | GotResult (Result Http.Error AutocompleteJson)
    | Map PickerMap.Msg


debounceConfig : Debounce.Config Msg
debounceConfig =
    { strategy = Debounce.later 1000
    , transform = DebounceSearch
    }


tipsFetchExec : String -> Cmd Msg
tipsFetchExec str =
    Task.perform TipsFetchExec (Task.succeed str)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Child messages interceptions
        Map childMsg ->
            case childMsg of
                PickerMap.SetCoords coords ->
                    let
                        ( updatedPickerModel, updatedPickerCmd ) =
                            PickerMap.update (PickerMap.SetCoords coords) model.map
                    in
                    ( { model | map = updatedPickerModel }, Cmd.map Map updatedPickerCmd )

                PickerMap.GotPoint result ->
                    case result of
                        Ok res ->
                            case List.head res.addresses of
                                Just addrEl ->
                                    let
                                        ( updatedPickerModel, updatedPickerCmd ) =
                                            PickerMap.update (PickerMap.SetAddress addrEl) model.map
                                    in
                                    case updatedPickerModel.address of
                                        Just address ->
                                            ( { model
                                                | map = updatedPickerModel
                                                , search =
                                                    case updatedPickerModel.address of
                                                        Just el ->
                                                            el.address.freeformAddress

                                                        --|> Debug.log "new address str"
                                                        Nothing ->
                                                            ""
                                              }
                                            , Cmd.batch
                                                [ Cmd.map Map updatedPickerCmd
                                                , addressToJS updatedPickerModel.address
                                                ]
                                            )

                                        Nothing ->
                                            ( { model | map = updatedPickerModel }, Cmd.none )

                                Nothing ->
                                    ( { model | error = "Empty result" }, Cmd.none )

                        Err error ->
                            let
                                e =
                                    error

                                --|> Debug.log "error"
                                ( updatedPickerModel, updatedPickerCmd ) =
                                    PickerMap.update (PickerMap.ToggleLoading False) model.map
                            in
                            ( { model | map = updatedPickerModel, error = "Getting point server error" }
                            , Cmd.map Map updatedPickerCmd
                            )

                PickerMap.SetAddress result ->
                    let
                        ( updatedPickerModel, updatedPickerCmd ) =
                            PickerMap.update (PickerMap.SetAddress result) model.map
                    in
                    ( { model | map = updatedPickerModel }, Cmd.map Map updatedPickerCmd )

                PickerMap.ToggleLoading bool ->
                    let
                        ( updatedPickerModel, _ ) =
                            PickerMap.update (PickerMap.ToggleLoading bool) model.map
                    in
                    ( { model | map = updatedPickerModel }, Cmd.none )

        -- Main module messages
        SearchChange str ->
            let
                ( debounce, cmd ) =
                    Debounce.push debounceConfig str model.debounce
            in
            ( { model | search = str, debounce = debounce }, cmd )

        DebounceSearch msg_ ->
            let
                ( debounce, cmd ) =
                    Debounce.update
                        debounceConfig
                        (Debounce.takeLast tipsFetchExec)
                        msg_
                        model.debounce
            in
            ( { model | debounce = debounce }, cmd )

        TipsFetchExec str ->
            if not (String.isEmpty str) then
                let
                    ( updatedPickerModel, updatedPickerCmd ) =
                        PickerMap.update (PickerMap.ToggleLoading True) model.map
                in
                ( { model | map = updatedPickerModel }
                , Cmd.batch [ fetchAddress (String.words str), Cmd.map Map updatedPickerCmd ]
                )

            else
                ( { model | tips = [], error = "" }, Cmd.none )

        TipClick t ->
            if not (checkIfTipIsEmpty t) then
                let
                    ( updatedModel, updMsg ) =
                        update (Map (PickerMap.SetAddress t)) { model | tips = [], search = PickerMap.showTipName t.address }
                in
                --{ model | tips = [], search =  PickerMap.showTipName t.address } |> update ( Map (PickerMap.SetCoords t.position) )
                ( updatedModel
                , Cmd.batch
                    [ updMsg
                    , addressToJS updatedModel.map.address
                    ]
                )

            else
                ( model, Cmd.none )

        GotResult result ->
            case result of
                Ok res ->
                    let
                        tips =
                            if List.length res.results == 0 then
                                [ emptyTip ]

                            else
                                res.results

                        --|> Debug.log "result"
                        ( updatedPickerModel, updatedPickerCmd ) =
                            PickerMap.update (PickerMap.ToggleLoading False) model.map
                    in
                    ( { model | map = updatedPickerModel, tips = tips, error = "" }
                    , Cmd.map Map updatedPickerCmd
                    )

                Err error ->
                    let
                        e =
                            error

                        --|> Debug.log "error"
                        ( updatedPickerModel, updatedPickerCmd ) =
                            PickerMap.update (PickerMap.ToggleLoading False) model.map
                    in
                    ( { model | map = updatedPickerModel, error = "Autocomplete response error" }
                    , Cmd.map Map updatedPickerCmd
                    )



-- OUTPUT PORTS


port addressToJS : Maybe PickerMap.AddressesArrElement -> Cmd msg



-- INPUT PORTS


port setCoords : (PickerMap.Coordinates -> msg) -> Sub msg



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    setCoords (Map << PickerMap.SetCoords)



-- VIEW


checkIfTipIsEmpty : PickerMap.AddressesArrElement -> Bool
checkIfTipIsEmpty tip =
    tip.type_ == emptyTip.type_


view : Model -> Html Msg
view model =
    div [ class "form-block" ]
        [ Html.map Map <| lazy PickerMap.view model.map
        , div [ class "dropdown" ]
            [ input
                [ type_ "text"
                , classList
                    [ ( "form-control", True )
                    , ( "dropdown-toggle", True )
                    , ( "autocomplete-input", True )
                    , ( "loading", model.map.loading )
                    ]
                , placeholder "Input address"
                , value model.search
                , onInput SearchChange
                , id "dropdownMenuLink"
                ]
                []
            , div
                [ classList
                    [ ( "dropdown-menu", True )
                    , ( "active", List.length model.tips > 0 )
                    ]
                , ariaLabel "dropdownMenuLink"
                ]
                (List.map
                    (\e ->
                        a
                            [ classList
                                [ ( "dropdown-item", not (checkIfTipIsEmpty e) )
                                , ( "dropdown-header", checkIfTipIsEmpty e )
                                ]
                            , Html.Events.custom "click"
                                (Decode.succeed
                                    { message = TipClick e
                                    , stopPropagation = True
                                    , preventDefault = True
                                    }
                                )
                            ]
                            [ text <| PickerMap.showTipName e.address ]
                    )
                    model.tips
                )
            ]
        , div [] [ text model.error ]
        ]
