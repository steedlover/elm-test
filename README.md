The Placepicker component based on [the TomTom map service](https://www.tomtom.com/)
# Install project
Install ELM globally:
```npm install -g elm```
OR for MAC users is preferable:
```brew install elm```
Install uglifyJS in case of using the compile script to prepare prod versions of the components
```npm install uglify-js -g```
```cd ../yourfolder```
```elm reactor```
## Start elements one by one
> not all of them work without connection with index.html
Navigate in browser [localhost:8000/src](http://localhost:8000/src) and choose any of the elements
## Start the whole project
> all the components work
### Compile the components with the script which includes iglifyJS
It's not allowed to use the debug.log function in the prod version 
```./compile.sh ./src Placepicker``` 
>Compile the placepicker component
---------------------------------
```./compile.sh ./src Multiverse```
> Compile the multiverse component
---------------------------------
```./compile.sh ./src Main```
> Compile the main component
---------------------------------
### Compile the components with the elm make command
```elm make ./src/Placepicker.elm --output ./dist/Placepicker.min.js```
> Compile the placepicker component
---------------------------------
```elm make ./src/Multiverse.elm --output ./dist/Multiverse.min.js```
> Compile the multiverse component
---------------------------------
```elm make ./src/Main.elm --output ./dist/Main.min.js```
> Compile the main component
---------------------------------
And navigate in browser [localhost:8000/index.html](http://localhost:8000/index.html)

